/*Table structure for table `promo_codes` */

DROP TABLE IF EXISTS `promo_codes`;

CREATE TABLE `promo_codes` (
  `code_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Code Id',
  `code` varchar(19) NOT NULL COMMENT 'Code',
  `who_activated` varchar(12) DEFAULT NULL COMMENT 'Who activated code',
  `mail_subject` varchar(200) NOT NULL,
  `mail_text` varchar(200) NOT NULL,
  `honor` int(6) DEFAULT '0',
  `arena_points` int(5) DEFAULT '0',
  PRIMARY KEY (`code_id`,`code`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Table structure for table `promo_codes_items` */

DROP TABLE IF EXISTS `promo_codes_items`;

CREATE TABLE `promo_codes_items` (
  `code_id` int(10) NOT NULL,
  `item_id` int(10) unsigned DEFAULT NULL,
  `item_count` int(10) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;