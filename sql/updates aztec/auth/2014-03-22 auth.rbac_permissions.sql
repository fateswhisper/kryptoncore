INSERT INTO `rbac_permissions` (`id`, `name`) VALUES
(1200, 'Command: gh add'),
(1201, 'Command: gh del'),
(1202, 'Command: gh list'),
(1203, 'Command: gh set'),
(1204, 'Command: gh leave'),
(1205, 'Command: gh');
