-- UNIT_FLAG_NOT_ATTACKABLE_1 for creature auctioneer in pvp zone
UPDATE `creature` SET `unit_flags`=128 WHERE  `guid`=213333;

-- modelid, scale and position for npc arena 1v1
UPDATE `creature_template` SET `modelid1`=26327, `scale`=0.3 WHERE  `entry`=50002;
UPDATE `creature` SET `position_x`=-1867.38, `position_y`=-4138.26, `position_z`=10.4622, `orientation`=4.97489 WHERE  `guid`=213320;
UPDATE `creature` SET `position_x`=-1865.04, `position_y`=-4136.98, `position_z`=10.7252, `orientation`=5.4452 WHERE  `guid`=213303;

--  Lament of the Highborne: Highborne Aura for Sylvanas
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=39048;
DELETE FROM `smart_scripts` WHERE `entryorguid`=39048 AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES 
(39048, 0, 0, 0, 54, 0, 100, 0, 0, 0, 0, 0, 11, 37090, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Sylvanas'' Lamenter - On Summoned - Cast Lament of the Highborne: Highborne Aura'),
(39048, 0, 1, 0, 54, 0, 100, 0, 0, 0, 0, 0, 4, 15095, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Sylvanas'' Lamenter - On Summoned - Play Sound Lament of the Highborne');

--  Worgen's Spite не должен скалироваться от спд
DELETE FROM `spell_bonus_data` WHERE `entry`=30564;
INSERT INTO `spell_bonus_data` (`entry`, `direct_bonus`, `dot_bonus`, `ap_bonus`, `ap_dot_bonus`, `comments`) VALUES
(30564, 0, 0, 0, 0, 'Item - Torment of the Worgen - Worgen\'s Spite');

-- Spell 30567,30550, 30562, 30557 не должны быть активны вне зоны Каражан
DELETE FROM `spell_area` WHERE `area`=3457 AND `spell` IN(30567,30550, 30562, 30557);
INSERT INTO `spell_area` (`spell`, `area`, `quest_start`, `quest_end`, `aura_spell`, `racemask`, `gender`, `autocast`, `quest_start_status`, `quest_end_status`) VALUES
(30550, 3457, 0, 0, 0, 0, 2, 0, 0, 0),
(30557, 3457, 0, 0, 0, 0, 2, 0, 0, 0),
(30562, 3457, 0, 0, 0, 0, 2, 0, 0, 0),
(30567, 3457, 0, 0, 0, 0, 2, 0, 0, 0);


-- Spell 40623, 40624, 40627, 40625, 40628, 40626 не должны быть активны вне зоны Острогорье
DELETE FROM `spell_area` WHERE `spell` IN (40623, 40624, 40627, 40625, 40628, 40626);
INSERT INTO `spell_area` (`spell`, `area`, `racemask`, `gender`, `autocast`, `quest_start_status`, `quest_end_status`) VALUES
(40623,3522,0,2,0,0,0),
(40624,3522,0,2,0,0,0),
(40627,3522,0,2,0,0,0),
(40628,3522,0,2,0,0,0),
(40625,3522,0,2,0,0,0),
(40626,3522,0,2,0,0,0);