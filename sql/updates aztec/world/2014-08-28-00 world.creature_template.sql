-- Add creature immune from MECHANIC_FEAR
UPDATE creature_template SET mechanic_immune_mask = mechanic_immune_mask+16 WHERE scriptname = 'npc_training_dummy';