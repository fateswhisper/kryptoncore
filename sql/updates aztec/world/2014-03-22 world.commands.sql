INSERT INTO `command` (`name`, `permission`, `help`) VALUES
('gh add', 1200, 'Syntax: .gh add\r\n\r\nCreate new GuildHouse. You must have a guild member selected with needed phase mask.'),
('gh del', 1201, 'Syntax: .gh del [guild_id]\r\n\r\nDelete GuildHouse of selected player or by guild ID.'),
('gh list', 1202, 'List all guild houses (not yet implemented)'),
('gh set', 1203, 'Syntax: .gh set [guild_rank]\n\nSet new coordinates to your GuildHouse teleport and minimal guild rank for use it.\n.gh set 0 - only leader can use teleport\n.gh set 1 - only leader and officers can use, etc.'),
('gh leave', 1204, 'Leave guildhouse.\n\nTeleports you to your previous location.'),
('gh', 1205, 'Syntax: .gh\r\n\r\nTeleports you to your GuildHouse');
