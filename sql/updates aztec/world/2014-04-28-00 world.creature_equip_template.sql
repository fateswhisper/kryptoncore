UPDATE `creature` SET `equipment_id`=0 WHERE  `id` in (50005, 50006);

DELETE from `creature_equip_template` where `entry` in (50005, 50006);
INSERT INTO `creature_equip_template` (`entry`, `id`, `itemEntry1`, `itemEntry2`, `itemEntry3`, `WDBVerified`) VALUES
(50006, 1, 43221, 0, 0, 18019),
(50005, 1, 42288, 0, 0, 18019);
