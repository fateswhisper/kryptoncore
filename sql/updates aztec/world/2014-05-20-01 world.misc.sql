-- dk deathcoil heal
UPDATE `spell_bonus_data` SET `ap_bonus`=0.3 WHERE  `entry`=47633;

-- mage ignite
DELETE FROM spell_script_names WHERE ScriptName = 'spell_mage_ignite';
INSERT INTO spell_script_names VALUES
(-11119, 'spell_mage_ignite');