DELETE FROM `spell_ranks` WHERE `first_spell_id` IN (57518, 12880);
INSERT INTO `spell_ranks` (`first_spell_id`,`spell_id`,`rank`) VALUES
('57518','57518','1'),
('57518','57519','2'),
('57518','57520','3'),
('57518','57521','4'),
('57518','57522','5'),
('12880','12880','1'),
('12880','14201','2'),
('12880','14202','3'),
('12880','14203','4'),
('12880','14204','5');

DELETE FROM `spell_group` WHERE `id`=1121;
INSERT INTO `spell_group` (`id`,`spell_id`) VALUES
('1121','57518'),
('1121','12880');

DELETE FROM `spell_group_stack_rules` WHERE `group_id`=1121;
INSERT INTO `spell_group_stack_rules` (`group_id`,`stack_rule`) VALUES ('1121','1');