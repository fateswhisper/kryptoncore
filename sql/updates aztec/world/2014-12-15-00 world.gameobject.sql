INSERT INTO `gameobject_template` (`entry`, `type`, `displayId`, `name`, `IconName`, `castBarCaption`, `unk1`, `faction`, `flags`, `size`, `questItem1`, `questItem2`, `questItem3`, `questItem4`, `questItem5`, `questItem6`, `data0`, `data1`, `data2`, `data3`, `data4`, `data5`, `data6`, `data7`, `data8`, `data9`, `data10`, `data11`, `data12`, `data13`, `data14`, `data15`, `data16`, `data17`, `data18`, `data19`, `data20`, `data21`, `data22`, `data23`, `AIName`, `ScriptName`, `WDBVerified`) VALUES
(300002, 5, 5411, 'Circle of Calling (by Crypton)', '', '', '', 0, 0, 1.3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 11723),
(300001, 5, 7593, 'Stairway to Mercy (by Crypton)', '', '', '', 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 12340);

SET @GUID := XXXX;
INSERT INTO `gameobject` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`) VALUES
(@GUID+1, 300002, 617, 1, 1, 1268.1, 814.76, 6.62836, 5.66365, 0, 0, 0.304838, -0.952404, 300, 0, 1),
(@GUID+2, 300002, 617, 1, 1, 1267.58, 815.129, 6.19505, 5.66365, 0, 0, 0.304838, -0.952404, 300, 0, 1),
(@GUID+3, 300002, 617, 1, 1, 1266.86, 815.64, 5.76194, 5.66365, 0, 0, 0.304838, -0.952404, 300, 0, 1),
(@GUID+4, 300002, 617, 1, 1, 1266.22, 816.098, 5.32758, 5.66365, 0, 0, 0.304838, -0.952404, 300, 0, 1),
(@GUID+5, 300002, 617, 1, 1, 1265.59, 816.55, 4.8945, 5.66365, 0, 0, 0.304838, -0.952404, 300, 0, 1),
(@GUID+6, 300002, 617, 1, 1, 1264.9, 817.039, 4.46114, 5.66365, 0, 0, 0.304838, -0.952404, 300, 0, 1),
(@GUID+7, 300002, 617, 1, 1, 1264.3, 817.467, 4.02677, 5.66365, 0, 0, 0.304838, -0.952404, 300, 0, 1),
(@GUID+8, 300002, 617, 1, 1, 1263.67, 817.919, 3.59339, 5.66365, 0, 0, 0.304838, -0.952404, 300, 0, 1),
(@GUID+9, 300002, 617, 1, 1, 1263.07, 818.348, 3.16004, 5.66365, 0, 0, 0.304838, -0.952404, 300, 0, 1),
(@GUID+10, 300001, 562, 1, 1, 6219.16, 239.721, 0.3, 2.37625, 0, 0, 0.927671, 0.373399, 300, 0, 1),
(@GUID+11, 300001, 562, 1, 1, 6258.63, 284.739, 0.35, 5.53143, 0, 0, 0.367087, -0.930187, 300, 0, 1),
(@GUID+12, 300002, 617, 1, 1, 1314.2, 767.979, 6.77999, 2.46355, 0, 0, 0.94308, 0.332566, 300, 0, 1),
(@GUID+13, 300002, 617, 1, 1, 1314.97, 767.358, 6.15843, 2.46355, 0, 0, 0.94308, 0.332566, 300, 0, 1),
(@GUID+14, 300002, 617, 1, 1, 1315.52, 766.911, 5.65871, 2.46355, 0, 0, 0.94308, 0.332566, 300, 0, 1),
(@GUID+15, 300002, 617, 1, 1, 1316.35, 766.242, 5.15896, 2.46355, 0, 0, 0.94308, 0.332566, 300, 0, 1),
(@GUID+16, 300002, 617, 1, 1, 1317.07, 765.667, 4.65922, 2.46355, 0, 0, 0.94308, 0.332566, 300, 0, 1),
(@GUID+17, 300002, 617, 1, 1, 1317.69, 765.167, 4.15952, 2.46355, 0, 0, 0.94308, 0.332566, 300, 0, 1),
(@GUID+18, 300002, 617, 1, 1, 1318.3, 764.673, 3.65978, 2.46355, 0, 0, 0.94308, 0.332566, 300, 0, 1),
(@GUID+19, 300002, 617, 1, 1, 1318.75, 764.313, 3.15993, 2.46355, 0, 0, 0.94308, 0.332566, 300, 0, 1),
(@GUID+20, 300002, 617, 1, 1, 1268.1, 814.76, 6.19505, 5.66365, 0, 0, 0.304838, -0.952404, 300, 0, 1),
(@GUID+21, 300002, 617, 1, 1, 1268.1, 814.76, 5.76194, 5.66365, 0, 0, 0.304838, -0.952404, 300, 0, 1),
(@GUID+22, 300002, 617, 1, 1, 1268.1, 814.76, 5.32758, 5.66365, 0, 0, 0.304838, -0.952404, 300, 0, 1),
(@GUID+23, 300002, 617, 1, 1, 1268.1, 814.76, 4.8945, 5.66365, 0, 0, 0.304838, -0.952404, 300, 0, 1),
(@GUID+24, 300002, 617, 1, 1, 1268.1, 814.76, 4.46114, 5.66365, 0, 0, 0.304838, -0.952404, 300, 0, 1),
(@GUID+25, 300002, 617, 1, 1, 1268.1, 814.76, 4.02677, 5.66365, 0, 0, 0.304838, -0.952404, 300, 0, 1),
(@GUID+26, 300002, 617, 1, 1, 1268.1, 814.76, 3.59339, 5.66365, 0, 0, 0.304838, -0.952404, 300, 0, 1),
(@GUID+27, 300002, 617, 1, 1, 1268.1, 814.76, 3.16004, 5.66365, 0, 0, 0.304838, -0.952404, 300, 0, 1),
(@GUID+28, 300002, 617, 1, 1, 1314.2, 767.979, 6.15843, 2.46355, 0, 0, 0.94308, 0.332566, 300, 0, 1),
(@GUID+29, 300002, 617, 1, 1, 1314.2, 767.979, 5.65871, 2.46355, 0, 0, 0.94308, 0.332566, 300, 0, 1),
(@GUID+30, 300002, 617, 1, 1, 1314.2, 767.979, 5.15896, 2.46355, 0, 0, 0.94308, 0.332566, 300, 0, 1),
(@GUID+31, 300002, 617, 1, 1, 1314.2, 767.979, 4.65922, 2.46355, 0, 0, 0.94308, 0.332566, 300, 0, 1),
(@GUID+32, 300002, 617, 1, 1, 1314.2, 767.979, 4.15952, 2.46355, 0, 0, 0.94308, 0.332566, 300, 0, 1),
(@GUID+33, 300002, 617, 1, 1, 1314.2, 767.979, 3.65978, 2.46355, 0, 0, 0.94308, 0.332566, 300, 0, 1),
(@GUID+34, 300002, 617, 1, 1, 1314.2, 767.979, 3.15993, 2.46355, 0, 0, 0.94308, 0.332566, 300, 0, 1);