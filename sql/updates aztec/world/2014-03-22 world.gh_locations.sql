CREATE TABLE IF NOT EXISTS `gh_locations` (
  `guildid` int(10) unsigned NOT NULL,
  `min_rank` smallint(5) unsigned NOT NULL,
  `position_x` float NOT NULL,
  `position_y` float NOT NULL,
  `position_z` float NOT NULL,
  `orientation` float NOT NULL,
  `map` smallint(5) unsigned NOT NULL,
  `zone` smallint(6) unsigned NOT NULL,
  `area` smallint(6) unsigned NOT NULL,
  `phase_mask` int(10) unsigned NOT NULL,
  `comment` varchar(200) NOT NULL DEFAULT '',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`guildid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;