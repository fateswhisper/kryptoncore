SET @GUID := XXXXX;

-- Added spawn, addon and spell area info for Lightningcaller Soo-met
DELETE FROM `creature` WHERE `id`=28107;
INSERT INTO `creature` (`guid`,`id`,`map`,`spawnMask`,`phaseMask`,`modelid`,`equipment_id`,`position_x`,`position_y`,`position_z`,`orientation`,`spawntimesecs`,`spawndist`,`currentwaypoint`,`curhealth`,`curmana`,`MovementType`,`npcflag`,`unit_flags`,`dynamicflags`) VALUES
(@GUID,28107,571,1,1,0,0,5116.65,5469.73,-91.7097,1.6057,300,0,0,117700,3809,0,0,0,0);

DELETE FROM `creature_template_addon` WHERE `entry`=28107;
INSERT INTO `creature_template_addon` (`entry`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES
(28107,0,0,65536,1,0,'52215');

DELETE FROM `spell_area` WHERE `spell`=52217 AND `area`=4288;
INSERT INTO `spell_area` (`spell`,`area`,`quest_start`,`quest_end`,`aura_spell`,`racemask`,`gender`,`autocast`,`quest_start_status`,`quest_end_status`) VALUES
(52217,4288,12705,12705,0,0,2,1,74,11),
(52217,4288,12761,12761,0,0,2,1,74,11),
(52217,4288,12762,12762,0,0,2,1,74,11);

-- Added spawn, addon and spell area info for Shaman Jakjek
DELETE FROM `creature` WHERE `id`=28106;
INSERT INTO `creature` (`guid`,`id`,`map`,`spawnMask`,`phaseMask`,`modelid`,`equipment_id`,`position_x`,`position_y`,`position_z`,`orientation`,`spawntimesecs`,`spawndist`,`currentwaypoint`,`curhealth`,`curmana`,`MovementType`,`npcflag`,`unit_flags`,`dynamicflags`) VALUES
(@GUID+1,28106,571,1,1,0,0,4891.31,5905.05,-40.6086,3.66024,300,0,0,117700,3809,0,0,0,0);

DELETE FROM `creature_template_addon` WHERE `entry`=28106;
INSERT INTO `creature_template_addon` (`entry`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES
(28106,0,0,65536,1,0,'52215');

DELETE FROM `spell_area` WHERE `spell`=52217 AND `area`=4287;
INSERT INTO `spell_area` (`spell`,`area`,`quest_start`,`quest_end`,`aura_spell`,`racemask`,`gender`,`autocast`,`quest_start_status`,`quest_end_status`) VALUES
(52217,4287,12703,12703,0,0,2,1,74,11),
(52217,4287,12759,12759,0,0,2,1,74,11),
(52217,4287,12760,12760,0,0,2,1,74,11);