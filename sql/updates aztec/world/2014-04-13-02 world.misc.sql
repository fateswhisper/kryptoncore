-- Immunities masks for Drakkari Colossus
UPDATE `creature_template` SET `mechanic_immune_mask`=`mechanic_immune_mask`|604706780 WHERE `entry` IN (
29307,
31365);

-- Fix Essence of the Pure Flame scaling
DELETE FROM `spell_bonus_data` WHERE `entry` = 23266;
INSERT INTO `spell_bonus_data` (`entry`, `direct_bonus`, `dot_bonus`, `ap_bonus`, `ap_dot_bonus`, `comments`) VALUES
(23266, 0, 0, 0, 0, 'Item - Essence of the Pure Flame no bonus');