DELETE FROM `creature_text` WHERE `entry`=17123;
INSERT INTO `creature_text` (`entry`, `groupid`, `id`, `text`, `type`, `language`, `probability`, `emote`, `duration`, `sound`, `comment`) VALUES
(17123,0,0,'Welcome, kind spirit. What has brought you to us?',12,0,100,0,0,0,'');

UPDATE `gossip_menu_option` SET `option_text`='Chief Ashtotem, I have misplaced the token that you gave me.' WHERE  `menu_id`=8898 AND `id`=0;