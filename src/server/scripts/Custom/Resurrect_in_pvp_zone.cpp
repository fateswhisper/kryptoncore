#include "ScriptPCH.h"
#include "Player.h"

class resurrect_in_pvp_zone : public PlayerScript
{
public:
        resurrect_in_pvp_zone() : PlayerScript("resurrect_in_pvp_zone") {}

        void OnPVPKill(Player * Killer, Player* Killed)
        {
            if (Killer->GetAreaId() == 2401)
            {
                if (Killed->HasAuraType(SPELL_AURA_SPIRIT_OF_REDEMPTION))
                    Killed->RemoveAurasByType(SPELL_AURA_MOD_SHAPESHIFT);

                Killed->KillPlayer();
                Killed->ResurrectPlayer(40.0f, true);

                if (Aura * aur = Killed->GetAura(15007))
                    aur->SetDuration(1*MINUTE*IN_MILLISECONDS);

                Killed->CastSpell(Killed, 13874, true);
                if (Aura * aur = Killed->GetAura(13874))
                    aur->SetDuration(1*MINUTE*IN_MILLISECONDS);

                Killed->TeleportTo(0, -1818.4f, -4148.6f, 26.6f, 4.27675f);
            }
        }
};

void AddSC_resurrect_in_pvp_zone()
{
    new resurrect_in_pvp_zone();
}