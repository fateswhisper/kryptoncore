# Copyright (C) 2008-2014 TrinityCore <http://www.trinitycore.org/>
#
# This file is free software; as a special exception the author gives
# unlimited permission to copy and/or distribute it, with or without
# modifications, as long as this notice is preserved.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

set(scripts_STAT_SRCS
  ${scripts_STAT_SRCS}
	Custom/Transmogrification.h
	Custom/Transmogrification.cpp
	Custom/Transmogrifier.cpp
	Custom/duel_reset.cpp
	Custom/npc_Arena_resetMMR.cpp
	Custom/custom_rates.cpp
	Custom/Reforging.cpp
    Custom/npc_arena1v1.cpp
    Custom/npc_arena1v1.h
	Custom/Arena_Scripts.cpp
    Custom/ICC_ring_restorer.cpp
    Custom/Resurrect_in_pvp_zone.cpp
    Custom/npc_promo_codes.cpp
)

message("  -> Prepared: Custom")
