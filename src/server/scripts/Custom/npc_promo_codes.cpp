#include "ScriptPCH.h"

class npc_promo_codes : public CreatureScript
{
public:
    npc_promo_codes() : CreatureScript("npc_promo_codes") { }


    bool OnGossipHello(Player* player, Creature* creature)
    {
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "���������� � ������� �����-�����.", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF);
        player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_INTERACT_1, "������ �����-���", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 1, "��������� ���?", 0, true);
        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());

        return true;
    }

    bool OnGossipSelect(Player* player, Creature* creature, uint32 sender, uint32 action) OVERRIDE
    {
        player->PlayerTalkClass->ClearMenus();

        if (sender != GOSSIP_SENDER_MAIN)
            return false;

        if (action == GOSSIP_ACTION_INFO_DEF)
        {
            creature->MonsterTextEmote("����� ��������������� ��������, ����� ������ ���������� ����� �����-��� � ����������� ����.", player, false);
        }

        player->CLOSE_GOSSIP_MENU();

        return true;
    }


    bool OnGossipSelectCode(Player* player, Creature * creature, uint32 sender, uint32 action, const char* code) OVERRIDE
    {
        player->PlayerTalkClass->ClearMenus();

        if (sender != GOSSIP_SENDER_MAIN)
            return false;

        if (action == GOSSIP_ACTION_INFO_DEF + 1)
        {
            if (!code)
                return false;

            std::string code_data = code;

            // ����������� ������. �� �������, ��� ����� ����� ��������
            CharacterDatabase.EscapeString(code_data);

            QueryResult result = CharacterDatabase.PQuery("SELECT `code_id`, `who_activated`, `mail_subject`, `mail_text`, `honor`, `arena_points` FROM `promo_codes` WHERE `code` = '%s' LIMIT 1", code_data.c_str());

            if (!result)
            {
                creature->MonsterTextEmote("������ ����� ���� �� ����������!", player, true);
                player->CLOSE_GOSSIP_MENU();
                return false;
            }

            Field* fields = result->Fetch();

            uint32 codeId = fields[0].GetUInt32();
            std::string who_activated = fields[1].GetString();
            std::string mail_subject = fields[2].GetString();
            std::string mail_text = fields[3].GetString();
            uint32 honor = fields[4].GetUInt32();
            uint32 arena_points = fields[5].GetUInt32();

            // ���������, ����������� �� ��� ���
            if (who_activated != "")
            {
                creature->MonsterTextEmote("����� ��� ��� �����������!", player, true);
                player->CLOSE_GOSSIP_MENU();
                return false;
            }
            else
            {
                CharacterDatabase.DirectPExecute("UPDATE `promo_codes` SET `who_activated` = '%s' WHERE `code_id` = '%u'", player->GetName().c_str(), codeId);
            }

            // ���������, ��������� �� ����� �� ����
            if (honor)
            {
                uint32 differenceHonor = sWorld->getIntConfig(CONFIG_MAX_HONOR_POINTS) - player->GetHonorPoints();

                if (differenceHonor < honor)
                    creature->MonsterTextEmote("� ��� ������� ����� ����� �����!", player, true);
                else
                {
                    player->ModifyHonorPoints(honor);
                    creature->MonsterTextEmote("�������� ���� ����� �� ����� ���!", player, true);
                }
            }

            // ���������� ��� � �������, ������ � ����
            if (arena_points)
            {
                uint32 differenceArenaPoints = sWorld->getIntConfig(CONFIG_MAX_ARENA_POINTS) - player->GetArenaPoints();

                if (differenceArenaPoints < arena_points)
                    creature->MonsterTextEmote("� ��� ������� ����� ����� �����!", player, true);
                else
                {
                    player->ModifyArenaPoints(arena_points);
                    creature->MonsterTextEmote("�������� ���� ����� �� ����� ���!", player, true);
                }
            }

            // ������ ���� ��� � ��������, ������������ � �������� ������ � ������� � ���� �����, � ����������
            MailDraft draft(mail_subject, mail_text);

            SQLTransaction trans = CharacterDatabase.BeginTransaction();

            result = CharacterDatabase.PQuery("SELECT `item_id`, `item_count` FROM `promo_codes_items` WHERE `code_id` = '%u'", codeId);

            if (result)
            {
                typedef std::pair<uint32, uint32> ItemPair;
                typedef std::list<ItemPair> ItemPairs;
                ItemPairs items;

                do
                {
                    Field* fields = result->Fetch();

                    uint32 itemId = fields[0].GetUInt32();
                    uint16 itemCount = fields[1].GetUInt16();

                    ItemTemplate const* itemProto = sObjectMgr->GetItemTemplate(itemId);
                    if (!itemProto)
                        continue;

                    if (itemCount < 1 || (itemProto->MaxCount > 0 && itemCount > uint32(itemProto->MaxCount)))
                        continue;

                    while (itemCount > itemProto->GetMaxStackSize())
                    {
                        items.push_back(ItemPair(itemId, itemProto->GetMaxStackSize()));
                        itemCount -= itemProto->GetMaxStackSize();
                    }

                    items.push_back(ItemPair(itemId, itemCount));

                    if (items.size() > MAX_MAIL_ITEMS)
                    {
                        player->CLOSE_GOSSIP_MENU();
                        return false;
                    }
                } while (result->NextRow());

                for (ItemPairs::const_iterator itr = items.begin(); itr != items.end(); ++itr)
                {
                    if (Item* item = Item::CreateItem(itr->first, itr->second, 0))
                    {
                        item->SaveToDB(trans);
                        draft.AddItem(item);
                    }
                }
                creature->MonsterTextEmote("�������� �� ����� ��� ���������� �� �����!", player, true);
            }

            draft.SendMailTo(trans, MailReceiver(player, GUID_LOPART(player->GetGUID())), MailSender(MAIL_NORMAL, 0, MAIL_STATIONERY_GM));
            CharacterDatabase.CommitTransaction(trans);

            player->CLOSE_GOSSIP_MENU();
        }
        return true;
    }
};

void AddSC_npc_promo_codes()
{
    new npc_promo_codes;
}