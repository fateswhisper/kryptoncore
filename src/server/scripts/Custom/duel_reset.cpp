#include "ScriptPCH.h"
#include "ScriptMgr.h"
#include "Config.h"

class duel_reset : public PlayerScript
{
    public:
        duel_reset() : PlayerScript("duel_reset") {}

    void OnDuelEnd(Player *winner, Player *looser, DuelCompleteType type)
    {
        // reset cooldowns in zone
        if (!sWorld->getBoolConfig(CONFIG_DUEL_RESET_COOLDOWN)) return;
		
		uint32 zone = winner->GetZoneId();

		//  dur         elwyn        gm isle         hyjal       	azshara crater	arathi highlands
		if (zone == 14 || zone == 12 || zone == 876 || zone == 616 || zone == 268 || zone == 45)
        {
            if (type == DUEL_WON)
            {
                winner->RemoveArenaSpellCooldowns();
                looser->RemoveArenaSpellCooldowns();
                winner->SetHealth(winner->GetMaxHealth());
                looser->SetHealth(looser->GetMaxHealth());

                if (winner->getPowerType() == POWER_MANA) 
                    winner->SetPower(POWER_MANA, winner->GetMaxPower(POWER_MANA));

                if (looser->getPowerType() == POWER_MANA) 
                    looser->SetPower(POWER_MANA, looser->GetMaxPower(POWER_MANA));
            }

            winner->HandleEmoteCommand(EMOTE_ONESHOT_CHEER);
        }
    }
};

void AddSC_DuelReset()
{
    new duel_reset;
}