/*
 * Copyright (C) 2008-2013 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2006-2009 ScriptDev2 <https://scriptdev2.svn.sourceforge.net/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/* ScriptData
SDName: npc_Arena_resetMMR
SD%Complete: 100
SDComment: Reset MMR
SDCategory: PVP_Arena
Author: Firka
EndScriptData */

#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "ScriptedGossip.h"
#include "Player.h"
#include <cstring>

#define GOSSIP_ITEM_3       "Сбросить MMR ддя брекета 1 vs 1"
#define GOSSIP_ITEM_1       "Сбросить MMR ддя брекета 2 vs 2"
#define GOSSIP_ITEM_2       "Сбросить MMR ддя брекета 3 vs 3"

class npc_Arena_resetMMR : public CreatureScript
{
    public:

        npc_Arena_resetMMR()
            : CreatureScript("npc_Arena_resetMMR")
        {
        }

    uint32 arenaPoints;
    uint32 actRating5vs5;
    uint32 actRating2vs2;
    uint32 actRating3vs3;

    bool OnGossipHello(Player* player, Creature* creature) OVERRIDE
        {
            arenaPoints = player->GetArenaPoints();   //get arenaPoints
            // Get actual MMR for teams
            actRating5vs5 = player->GetArenaMatchMakerRating(player->GetGUID(),2);
            actRating2vs2 = player->GetArenaMatchMakerRating(player->GetGUID(),0);
            actRating3vs3 = player->GetArenaMatchMakerRating(player->GetGUID(),1);
            //--------------------------------

            std::ostringstream AP;
            std::ostringstream team5;
            std::ostringstream team2;
            std::ostringstream team3;
            AP    << "Текущие очки арены: " << arenaPoints;
            team5 << "Текущий рейтинг MMR для брекета 1vs1: " << actRating5vs5;
            team2 << "Текущий рейтинг MMR для брекета 2vs2: " << actRating2vs2;
            team3 << "Текущий рейтинг MMR для брекета 3vs3: " << actRating3vs3;

            creature->MonsterWhisper(AP.str().c_str(), player,false);
            creature->MonsterWhisper(team5.str().c_str(), player,false);          
            creature->MonsterWhisper(team2.str().c_str(), player,false);         
            creature->MonsterWhisper(team3.str().c_str(), player,false);           

            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, GOSSIP_ITEM_3, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+3);
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, GOSSIP_ITEM_1, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+1);
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, GOSSIP_ITEM_2, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+2);

            player->PlayerTalkClass->SendGossipMenu(907, creature->GetGUID());

            return true;
        }

    bool OnGossipSelect(Player* player, Creature* creature, uint32 /*sender*/, uint32 action) OVERRIDE
        {
            player->PlayerTalkClass->ClearMenus();         
            std::ostringstream resultOk;
            uint32 minValueForResetMMR   	= 1800;            //minimum value for reset MMR
            uint32 setValueMMR         	= 1500;            //value for setup   
            uint32 priceAP            		= 3500;            //prece in ArenaPoints

            // if team = 0  to no team
            uint32 team5vs5 =  player->GetArenaTeamId(2);
            uint32 team2vs2 =  player->GetArenaTeamId(0);
            uint32 team3vs3 =  player->GetArenaTeamId(1);
            //--------------------------------   

            if(arenaPoints >=priceAP)
            {               
                resultOk << "Ваш MMR сброшен до " << setValueMMR;

                switch (action)
                {
                    case GOSSIP_ACTION_INFO_DEF+3:               
                        if(actRating5vs5 > minValueForResetMMR)
                            if(team5vs5 == 0)
                            {
                                player->SetArenaMatchMakerRating(player->GetGUID(),2,setValueMMR);
                                creature->MonsterWhisper(resultOk.str().c_str(),player,false);   
                                player->SetArenaPoints(arenaPoints-priceAP);
                            }
                            else
                                creature->MonsterWhisper("У вас есть активная команда 1 vs 1",player,false);
                                player->CLOSE_GOSSIP_MENU();
                            return true; 

                    case GOSSIP_ACTION_INFO_DEF+1:   
                        if(actRating2vs2 > minValueForResetMMR)
                            if(team2vs2 == 0)
                            {
                                player->SetArenaMatchMakerRating(player->GetGUID(),0,setValueMMR);                           
                                creature->MonsterWhisper(resultOk.str().c_str(),player,false);   
                                player->SetArenaPoints(arenaPoints-priceAP);
                            }
                            else
                                creature->MonsterWhisper("У вас есть активная команда 2 vs 2",player,false);
                                player->CLOSE_GOSSIP_MENU();
                            return true;

					case GOSSIP_ACTION_INFO_DEF+2:               
						if(actRating3vs3 > minValueForResetMMR)
                            if(team3vs3 == 0)
                            {
                                player->SetArenaMatchMakerRating(player->GetGUID(),1,setValueMMR);
                                creature->MonsterWhisper(resultOk.str().c_str(),player,false);
                                player->SetArenaPoints(arenaPoints-priceAP);
                            }
                            else
                                creature->MonsterWhisper("У вас есть активная команда 3 vs 3",player,false);
                                player->CLOSE_GOSSIP_MENU();
                            return true;
                }

                player->SetDisplayId(30414); //превращение в панду
            }
            else
            {
                resultOk << "Вам нужно очков арены >= " << priceAP;
                creature->MonsterWhisper(resultOk.str().c_str(),player,false);
                player->CLOSE_GOSSIP_MENU();
            }
            return true;
        }

    bool OnGossipSelectCode(Player* player, Creature* creature, uint32 sender, uint32 action, char const* code) OVERRIDE
        {
            player->CLOSE_GOSSIP_MENU();
            return false;
        }
};

void AddSC_npc_Arena_resetMMR()
{
    new npc_Arena_resetMMR();
}