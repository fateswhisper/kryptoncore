/*
 * Copyright (C) 2008-2014 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/* ScriptData
Name: tele_commandscript
%Complete: 100
Comment: All tele related commands
Category: commandscripts
EndScriptData */

#include "Chat.h"
#include "Group.h"
#include "Language.h"
#include "MapManager.h"
#include "ObjectMgr.h"
#include "Player.h"
#include "ScriptMgr.h"
#include "GuildMgr.h"

class gh_commandscript : public CommandScript
{
public:
    gh_commandscript() : CommandScript("gh_commandscript") { }

    ChatCommand* GetCommands() const OVERRIDE
    {
        static ChatCommand ghCommandTable[] =
        {
            { "add",   rbac::RBAC_PERM_COMMAND_GH_ADD,  false, &HandleGHAddCommand,   "", NULL },
            { "del",   rbac::RBAC_PERM_COMMAND_GH_DEL,  false, &HandleGHDeleteCommand,"", NULL },
            { "list",  rbac::RBAC_PERM_COMMAND_GH_LIST, true,  &HandleGHListCommand,  "", NULL },
            { "set",   rbac::RBAC_PERM_COMMAND_GH_SET,  false, &HandleGHSetCommand,   "", NULL },
            { "leave", rbac::RBAC_PERM_COMMAND_GH_LEAVE,false, &HandleGHLeaveCommand, "", NULL },
            { "",      rbac::RBAC_PERM_COMMAND_GH,      false, &HandleGHCommand,      "", NULL },
            { NULL,    0,                               false, NULL,                  "", NULL }
        };
        static ChatCommand commandTable[] =
        {
            { "gh", rbac::RBAC_PERM_COMMAND_GH,      false, NULL, "", ghCommandTable },
            { NULL,   0,                             false, NULL, "", NULL }
        };
        return commandTable;
    }

	static bool ShowErrMsg(ChatHandler* handler, int32 err_text)
	{
		handler->SendSysMessage(err_text);
        handler->SetSentErrorMessage(true);
        return false;
	}

	// .gh add gh teleport by coord/phase of selected player
    static bool HandleGHAddCommand(ChatHandler* handler, const char* args)
    {
        Player* player = handler->GetSession()->GetPlayer();
        if (!player) return false;

		uint32 guild_id = 0;
		uint32 gh_phase = PHASEMASK_NORMAL;

		// guild
		Player* p = handler->getSelectedPlayer();
		if (!p) return false;
		guild_id = p->GetGuildId();
		gh_phase = p->GetPhaseMask();

		if ((!guild_id)||(!sGuildMgr->GetGuildById(guild_id))) return ShowErrMsg(handler, LANG_COMMAND_GH_UNK_GUILD);

		GuildHouseTele ght;

		ght.mapId = player->GetMapId();
		ght.position_x  = player->GetPositionX();
        ght.position_y  = player->GetPositionY();
        ght.position_z  = player->GetPositionZ();
        ght.orientation = player->GetOrientation();
		uint32 zone, area;
		player->GetZoneAndAreaId(zone, area);
		ght.zoneId = zone;
		ght.areaId = area;
		ght.minGuildRank = 63; // 0 -> guild leader, 1 - officer, etc..
		ght.phaseMask = gh_phase;

		if (sObjectMgr->AddGHTele(guild_id, ght))
        {
            handler->SendSysMessage(LANG_COMMAND_GH_ADD_OK);
			return true;
        }

        return ShowErrMsg(handler, LANG_COMMAND_GH_ADD_ERROR);
    }

	static bool HandleGHDeleteCommand(ChatHandler* handler, const char* args)
    {
        Player* player = handler->GetSession()->GetPlayer();
        if (!player) return false;

		uint32 guild_id = 0;

		// guild
		if (*args) guild_id = atoi(args);
		else
		{
			Player* p = handler->getSelectedPlayer();
			if (!p) return false;
			guild_id = p->GetGuildId();
		}

		if ((!guild_id)||(!sGuildMgr->GetGuildById(guild_id))) return ShowErrMsg(handler, LANG_COMMAND_GH_UNK_GUILD);

		if (!sObjectMgr->DeleteGHTele(guild_id, true)) return ShowErrMsg(handler, LANG_COMMAND_GH_NOGH);
        handler->SendSysMessage(LANG_COMMAND_GH_DELETED);
        return true;
    }

	static bool HandleGHListCommand(ChatHandler* handler, const char* args)
    {
		// TODO:

        return true;
    }

	// .gh set [minimal guild_rank]
	// update guildhouse teleport point and minimal guild rank
    static bool HandleGHSetCommand(ChatHandler* handler, const char* args)
    {
        Player* player = handler->GetSession()->GetPlayer();
        if (!player) return false;

		// have guild ?
		uint32 guild_id = player->GetGuildId();
		if (!guild_id) return ShowErrMsg(handler, LANG_COMMAND_GH_NOGUILD);

		// have guildhouse?
		GuildHouseTele const* ght = sObjectMgr->GetGHTele(guild_id);
		if (!ght) return ShowErrMsg(handler, LANG_COMMAND_GH_NOGH);

		Guild* guild = sGuildMgr->GetGuildById(guild_id);
		if (!guild) return ShowErrMsg(handler, LANG_COMMAND_GH_NOGUILD); // ??!!

		// guildmaster?
		if (player->GetGUID() != guild->GetLeaderGUID()) return ShowErrMsg(handler, LANG_COMMAND_GH_NOLEADER);

		// guildmaster can't change location!!!
		uint32 mapId = player->GetMapId();
		uint32 zone, area;
		player->GetZoneAndAreaId(zone, area);

		if ((ght->areaId != area)||(ght->zoneId != zone)||(ght->mapId != mapId)) return ShowErrMsg(handler, LANG_COMMAND_GH_NOCHAREA);

		uint32 new_rank = (*args) ? atoi(args) : ght->minGuildRank;

		if (!sObjectMgr->UpdateGHTele(guild_id, player->GetPositionX(), player->GetPositionY(), player->GetPositionZ(), player->GetOrientation(), new_rank))
			return ShowErrMsg(handler, LANG_COMMAND_GH_SET_ERROR);			

		handler->SendSysMessage(LANG_COMMAND_GH_SET_OK);
		return true;
    }

    // teleport player to guildhouse
    static bool HandleGHCommand(ChatHandler* handler, const char* args)
    {
        Player* player = handler->GetSession()->GetPlayer();
        if (!player) return false;

		if (!player->IsAlive()) return true; // do not teleport dead

		// have guild ?
		uint32 guild_id = player->GetGuildId();
		if (!guild_id) return ShowErrMsg(handler, LANG_COMMAND_GH_NOGUILD);

		// have guildhouse?
		GuildHouseTele const* ght = sObjectMgr->GetGHTele(guild_id);
		if (!ght) return ShowErrMsg(handler, LANG_COMMAND_GH_NOGH);

		if (player->GetRank() > ght->minGuildRank) return ShowErrMsg(handler, LANG_COMMAND_GH_MINRANK); // 0 -> guild leader, 1 - officer, etc..

        if (player->IsInCombat()) return ShowErrMsg(handler, LANG_YOU_IN_COMBAT);

		if ((ght->mapId == player->GetMapId()) && (ght->zoneId == player->GetZoneId()) && (ght->areaId == player->GetAreaId())) return ShowErrMsg(handler, LANG_COMMAND_GH_ALREADY_INGH);

        // stop flight if need
        if (player->IsInFlight())
        {
            player->GetMotionMaster()->MovementExpired();
            player->CleanupAfterTaxiFlight();
        }
        // save only in non-flight case
        else player->SaveRecallPosition();

		// just for fun
		player->CastSpell(player, 14867, true);                  //spell: "Untalent Visual Effect"

        if (!player->TeleportTo(ght->mapId, ght->position_x, ght->position_y, ght->position_z, ght->orientation)) return false;
		handler->SendSysMessage(LANG_COMMAND_GH_TELEPORTED);

		return true;
    }

	// teleport player back from guildhouse to recall position
    static bool HandleGHLeaveCommand(ChatHandler* handler, const char* args)
    {
        Player* player = handler->GetSession()->GetPlayer();
        if (!player) return false;

		if (!player->IsAlive()) return true; // do not teleport dead

		// have guild ?
		uint32 guild_id = player->GetGuildId();
		if (!guild_id) return ShowErrMsg(handler, LANG_COMMAND_GH_NOGUILD);

        if (player->IsInCombat()) return ShowErrMsg(handler, LANG_YOU_IN_COMBAT);

		if (player->IsBeingTeleported()) return false;

		// have guildhouse?
		GuildHouseTele const* ght = sObjectMgr->GetGHTele(guild_id);
		if (!ght) return ShowErrMsg(handler, LANG_COMMAND_GH_NOGH);

		// all guildranks may leave gh?
		//if (player->GetRank() > ght->minGuildRank) return ShowErrMsg(handler, LANG_COMMAND_GH_MINRANK); // 0 -> guild leader, 1 - officer, etc..

		// in gh?
		if ((ght->mapId != player->GetMapId()) || (ght->zoneId != player->GetZoneId()) || (ght->areaId != player->GetAreaId())) return ShowErrMsg(handler, LANG_COMMAND_GH_NOT_INGH);

        // stop flight if need
        if (player->IsInFlight())
        {
            player->GetMotionMaster()->MovementExpired();
            player->CleanupAfterTaxiFlight();
        }

        if (!player->TeleportTo(player->m_recallMap, player->m_recallX, player->m_recallY, player->m_recallZ, player->m_recallO)) return false;
		handler->SendSysMessage(LANG_COMMAND_GH_LEAVE);

		return true;
    }
};

void AddSC_guildhouse_commandscript()
{
    new gh_commandscript();
}
